@file:Suppress("DEPRECATION")

package com.muhamadzain.rxjava_kotlin.ui.home

import android.app.ProgressDialog
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muhamadzain.rxjava_kotlin.R
import com.muhamadzain.rxjava_kotlin.base.BaseActivity
import com.muhamadzain.rxjava_kotlin.dataclass.Users

@Suppress("UNCHECKED_CAST", "CAST_NEVER_SUCCEEDS", "DEPRECATION")
class HomeActivity : BaseActivity() , HomeContract.View{

    private lateinit var presenter: HomePresenter
    private var progressDialog: ProgressDialog? = null
    private lateinit var adapter: HomeAdapter
    private lateinit var btnLoadUsers: Button
    private lateinit var rvUsers : RecyclerView

    private fun initComponents(){
        btnLoadUsers = findViewById(R.id.btn_load_users)
        rvUsers = findViewById(R.id.rvUsers)
    }

    private fun initEventListener(){
        btnLoadUsers.setOnClickListener {
            getUsers()
        }
    }

    private fun getUsers() {
        presenter.fetchData()
    }

    override fun getLayoutResource(): Int = R.layout.activity_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = HomePresenter(this)
        setContentView(R.layout.activity_home)
        initComponents()
        initEventListener()
    }

    override fun onRetrieve(user: List<Users.UsersItem?>?) {
        rvUsers.layoutManager = LinearLayoutManager(this)
        adapter = HomeAdapter(this, user as ArrayList<Users.UsersItem>)
        rvUsers.adapter = adapter
    }

    override fun onReject(string: String) {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        this.progressDialog = ProgressDialog.show(this, "","Loading....", true, false)
    }

    override fun hideLoading() {
        this.progressDialog?.dismiss()
    }
}