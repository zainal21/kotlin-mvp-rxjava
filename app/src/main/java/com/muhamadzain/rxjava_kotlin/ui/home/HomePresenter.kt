package com.muhamadzain.rxjava_kotlin.ui.home

import android.util.Log
import com.muhamadzain.rxjava_kotlin.network.Api
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class HomePresenter(
    private val view : HomeContract.View
) : HomeContract.Presenter{
    private val mCompositeDisposable : CompositeDisposable?

    init {
        mCompositeDisposable = CompositeDisposable()
    }

    override fun fetchData() {
        view.showLoading()
        val disposable = Api.endpoint.getUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({
                view.onRetrieve(it.users)
                view.hideLoading()
            },{
                view.hideLoading()
                view.onReject(it.toString())
                Log.e("error", it.toString())
            })
        mCompositeDisposable?.add(disposable)
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
        mCompositeDisposable?.clear()
    }
}