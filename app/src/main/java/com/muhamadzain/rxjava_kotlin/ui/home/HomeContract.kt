package com.muhamadzain.rxjava_kotlin.ui.home

import com.muhamadzain.rxjava_kotlin.base.BaseContract
import com.muhamadzain.rxjava_kotlin.base.BasePresenter
import com.muhamadzain.rxjava_kotlin.dataclass.Users

interface HomeContract {
    interface View : BaseContract{
        fun onRetrieve(user: List<Users.UsersItem?>?)
        fun onReject(string : String)
    }

    interface Presenter : HomeContract,BasePresenter{
        fun fetchData()
    }
}