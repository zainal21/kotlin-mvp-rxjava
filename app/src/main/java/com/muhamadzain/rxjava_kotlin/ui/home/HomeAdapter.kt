package com.muhamadzain.rxjava_kotlin.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.muhamadzain.rxjava_kotlin.R
import com.muhamadzain.rxjava_kotlin.dataclass.Users

class HomeAdapter(
    private val context: Context,
    private val users : ArrayList<Users.UsersItem>) : RecyclerView.Adapter<HomeAdapter.MyViewHolder>()
{
    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var usersName: TextView = view.findViewById(R.id.usersItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val viewItem = LayoutInflater.from(parent.context).inflate(R.layout.item_users, parent, false)
        return MyViewHolder(viewItem)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val usersData = users[position]
        holder.usersName.text = usersData.name
    }

    override fun getItemCount(): Int{
        return users.size
    }
}
