package com.muhamadzain.rxjava_kotlin.network

import com.muhamadzain.rxjava_kotlin.dataclass.Users
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiInterface {
    @GET("/users")
    fun getUsers() : Observable<Users>

}