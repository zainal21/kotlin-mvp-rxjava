package com.muhamadzain.rxjava_kotlin.dataclass

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Users(
	@field:SerializedName("Users")
	val users: List<UsersItem?>? = null
){
	@Keep
	data class UsersItem(

		@field:SerializedName("createdAt")
		val createdAt: String? = null,

		@field:SerializedName("name")
		val name: String? = null,

		@field:SerializedName("avatar")
		val avatar: String? = null,

		@field:SerializedName("id")
		val id: String? = null
	)
}


