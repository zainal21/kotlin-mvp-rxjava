package com.muhamadzain.rxjava_kotlin.base

interface BasePresenter {
    fun subscribe()
    fun unsubscribe()
}