package com.muhamadzain.rxjava_kotlin.base

interface BaseContract {
    fun showLoading()
    fun hideLoading()
}